import argparse
import os

# Example command: python merge_labels_classes.py -sl ../ml-3/dataset/traffic/labels/ -sc ../ml-3/dataset/traffic/labels/classes.txt -tc ../ml-3/dataset/classes.txt
 
parser = argparse.ArgumentParser()
requiredArguments = parser.add_argument_group("required arguments")
requiredArguments.add_argument("-sl", "--sourcelabelsdir", type=str, required=True, help="Directory containing the source labels")
requiredArguments.add_argument("-sc", "--sourceclasses", type=str, required=True, help="File containing the source classes")
requiredArguments.add_argument("-tc", "--targetclasses", type=str, required=True, help="File containing the target classes")
args = parser.parse_args()

sourceClasses = []
with open(args.sourceclasses) as f:
    sourceClasses = f.read().splitlines()

targetClasses = []
with open(args.targetclasses) as f:
    targetClasses = f.read().splitlines()

print('Source classes: ', sourceClasses)
print('Target classes: ', targetClasses)


for filename in os.listdir(args.sourcelabelsdir):
    if filename != 'classes.txt' and filename.endswith(".txt"):

        annotations = []
        newAnnotations = []

        with open(os.path.join(args.sourcelabelsdir, filename), 'r') as f:
            annotations = f.read().splitlines()
            
            for an in annotations:
                data = an.split(' ', 1)
                classIndex = int(data[0])
                className = sourceClasses[classIndex]

                targetClassIndex = targetClasses.index(className)

                data[0] = str(targetClassIndex)

                newAnnot = ' '.join([str(elem) for elem in data])

                newAnnotations.append(newAnnot)
            
        with open(os.path.join(args.sourcelabelsdir, filename), 'w') as out_file:
                for ann in newAnnotations:
                    out_file.write("{}\n".format(ann))




