
ML3-BrainsForBuildings1 - v1 2021-10-09 10:48am
==============================

This dataset was exported via roboflow.ai on October 9, 2021 at 8:49 AM GMT

It includes 397 images.
Cubes-lights-monitors-troops are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 416x416 (Fit within)

No image augmentation techniques were applied.


