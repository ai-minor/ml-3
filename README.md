# ML-3

Detect our favorite objects using YOLOv5.

Objects:
- Monitors
- Traffic lights
- Clone troopers
- Rubics cubes

Environment: [Google Colab](https://colab.research.google.com/drive/1BVglcnTsBA7dL2cJkpsxFUqCONZxeQTk#scrollTo=meo846msTczM)
